package uet.invincible.wabo.mission;


import android.util.Log;

import com.fpt.robot.Robot;
import com.fpt.robot.RobotException;
import com.fpt.robot.motion.RobotMotionLocomotionController;
import com.fpt.robot.motion.RobotMotionStiffnessController;
import com.fpt.robot.types.RobotMoveTargetPosition;
import com.fpt.robot.vision.RobotObjectDetection.ObjectPose;
import com.fpt.robot.wabo.WaboArm;
import com.fpt.robot.wabo.WaboSMAC;

public class WaboUtils {
	public static boolean moveCloserObject(ObjectPose pose,int id,Robot robot) {
		float x = pose.x;
		float y = pose.y;
		@SuppressWarnings("unused")
		float theta = pose.theta * -1;
		double distance = Math.sqrt(x * x + y * y);
		try {
			boolean result = false;
			if (distance > 0.7f) {
				Log.e("Wabo Bottono","move to x-0.7");
				result = RobotMotionLocomotionController.moveTo(robot, new RobotMoveTargetPosition(x - 0.7f, 0, 0.0f));
				Log.e("Wabo Bottono","move to x-0.7 "+result);
			}
			//move to near object
			Log.e("Wabo Bottono","move closer to object, id: " + id);
			result = WaboSMAC.moveCloserToObject(robot, id);
			Log.e("Wabo Bottono","moveCloserToObject " + result);
			//move more 10cm
			//result = RobotMotionLocomotionController.moveTo(robot,
			//		new RobotMoveTargetPosition(0.07f, 0, 0.0f));
			
			//result = RobotMotionLocomotionController.moveStop(robot);
			Log.e("Wabo Bottono","move more  " + result);
			return result;
		} catch (RobotException e) {
			Log.e("Wabo Bottono", "Move closer exeption: " + e.getMessage());
			e.printStackTrace();
		}
		return false;
	}
}
